# Colors

declare-option str rasmus_bg 'rgb:1a1a19'
declare-option str rasmus_fg 'rgb:d1d1d1'
declare-option str rasmus_black 'rgb:333332'
declare-option str rasmus_red 'rgb:ff968c'
declare-option str rasmus_green 'rgb:61957f'
declare-option str rasmus_yellow 'rgb:ffc591'
declare-option str rasmus_blue 'rgb:8db4d4'
declare-option str rasmus_magenta 'rgb:de9bc8'
declare-option str rasmus_cyan 'rgb:7bb099'
declare-option str rasmus_white 'rgb:d1d1d1'
declare-option str rasmus_bright_black 'rgb:4c4c4b'
declare-option str rasmus_bright_red 'rgb:ffafa5'
declare-option str rasmus_bright_green 'rgb:7aae98'
declare-option str rasmus_bright_yellow 'rgb:ffdeaa'
declare-option str rasmus_bright_blue 'rgb:a6cded'
declare-option str rasmus_bright_magenta 'rgb:f7b4e1'
declare-option str rasmus_bright_cyan 'rgb:94c9b2'
declare-option str rasmus_bright_white 'rgb:eaeaea'

# Unset Colors

declare-option str rasmus_bright_bg 'rgb:303030'
declare-option str rasmus_dim_fg 'rgb:A8A8A8'
declare-option str rasmus_alt_fg 'rgb:87AFAF'
declare-option str rasmus_alt_bg 'rgb:1C1C1C'
declare-option str rasmus_info_bg 'rgb:262626'
declare-option str rasmus_error_bg 'rgb:AF5F00'
declare-option str rasmus_sel_bg_p 'rgba:5FAFAF32'
declare-option str rasmus_sel_bg_s 'rgba:87AFD716'

# Code

set-face global value                "%opt{rasmus_red}"
set-face global type                 "%opt{rasmus_yellow}"
set-face global variable             "%opt{rasmus_yellow}"
set-face global module               "%opt{rasmus_blue}"
set-face global function             "%opt{rasmus_bright_white}+b"
set-face global string               "%opt{rasmus_yellow}"
set-face global keyword              "%opt{rasmus_blue}"
set-face global operator             "%opt{rasmus_bright_magenta}"
set-face global attribute            "%opt{rasmus_bright_magenta}"
set-face global comment              "%opt{rasmus_dim_fg}"
set-face global documentation        "%opt{rasmus_dim_fg}"
set-face global meta                 "%opt{rasmus_cyan}"
set-face global builtin              "%opt{rasmus_bright_yellow}+b"

# Markup

set-face global title                "%opt{rasmus_yellow}+b"
set-face global header               "%opt{rasmus_yellow}+b"
set-face global block                "%opt{rasmus_bright_blue}"
set-face global bullet               "%opt{rasmus_blue}"
set-face global list                 "%opt{rasmus_bright_blue}"
set-face global link                 "%opt{rasmus_bright_blue}"
set-face global mono                 "%opt{rasmus_bright_green}"

# Builtin Faces

set-face global Default              "%opt{rasmus_fg},%opt{rasmus_bg}"

# set-face global PrimarySelection     "default,%opt{rasmus_sel_bg_p}"
# set-face global SecondarySelection   "default,%opt{rasmus_sel_bg_s}"
# set-face global PrimaryCursor        "%opt(Default},%opt{Default}+fg"
# set-face global SecondaryCursor      "%opt{Default},%opt{Default}+fg"
# set-face global PrimaryCursorEol     "%opt{rasmus_black},%opt{rasmus_fg}+fg"
# set-face global SecondaryCursorEol   "%opt{rasmus_black},%opt{rasmus_bright_black}+fg"

set-face global MenuForeground       "%opt{rasmus_bg},%opt{rasmus_cyan}"
set-face global MenuInfo             "%opt{rasmus_cyan}"
set-face global MenuBackground       "%opt{rasmus_cyan},%opt{rasmus_bg}"
set-face global Information          "%opt{rasmus_cyan},%opt{rasmus_bg}"
set-face global Error                "%opt{rasmus_bright_white},%opt{rasmus_error_bg}"

set-face global StatusLine           "%opt{rasmus_dim_fg},%opt{rasmus_alt_bg}"
set-face global StatusLineInfo       "%opt{rasmus_blue}"
set-face global StatusLineMode       "%opt{rasmus_bg},%opt{rasmus_bright_blue}"
set-face global StatusLineValue      "%opt{rasmus_bg},%opt{rasmus_bright_blue}"
# set-face global StatusCursor         "%opt{rasmus_bg},%opt{rasmus_bright_blue}"
set-face global Prompt               "%opt{rasmus_fg},%opt{rasmus_bright_blue}"

set-face global LineNumbers          "%opt{rasmus_dim_fg}"
set-face global LineNumberCursor     "%opt{rasmus_fg}"
set-face global LineNumbersWrapped   "%opt{rasmus_bright_bg}"

set-face global BufferPadding        "%opt{rasmus_bright_bg}"
set-face global Whitespace           "%opt{rasmus_bright_bg}+f"
set-face global WrapMarker           Whitespace
set-face global MatchingChar         "%opt{rasmus_magenta},%opt{rasmus_bright_bg}+bF"
